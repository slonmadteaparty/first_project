import pygame
import random
pygame.init()

SCREEN_H = 1200
SCREEN_W = 1200
NUMB_OF_CELLS_H = 40
NUMB_OF_CELLS_W = 40
CELL_SIZE = SCREEN_W / NUMB_OF_CELLS_W
DISPLAY = "civ"
GROUND = "#00AA00"
WATER = "#0000FF"
FOREST = "#003300"
MOUNTAIN = "#555555"

class Scout(pygame.sprite.Sprite):
    def __init__(self, x, y, border=2):
        pygame.sprite.Sprite.__init__(self)
        self.border = border
        self.x = x + self.border
        self.y = y + self.border
        self.cell_scout = CELL_SIZE - 2*self.border
        self.image = pygame.Surface((self.cell_scout, self.cell_scout))
        self.image.fill(pygame.Color('#ff0000'))
        self.rect = pygame.Rect(self.x, self.y, self.cell_scout, self.cell_scout)
        self.select = False
        print(self.rect)

    def load_image(self):
        #print(self.select)
        if self.select:
            self.image = pygame.image.load("scout_select.png")
        else:
            self.image = pygame.Surface((self.cell_scout, self.cell_scout))
            self.image.fill(pygame.Color('#ff0000'))
        
          

    def click(self, key, pos):
        if key == 1:
            if self.point_in_object(pos):
                self.select = True
            else:
                self.select = False
            self.load_image()
        elif key == 3:
            if self.select:
                self.warp(pos)

    def warp(self, pos):
        self.rect.x = pos[0] / CELL_SIZE * CELL_SIZE + self.border
        self.x = pos[0] / CELL_SIZE * CELL_SIZE + self.border
        self.rect.y = pos[1] / CELL_SIZE * CELL_SIZE + self.border
        self.y = pos[1] / CELL_SIZE * CELL_SIZE + self.border

    def point_in_object(self, pos):
        if ((self.x <= pos[0] <= self.x + self.cell_scout)and(self.y <= pos[1] <= self.y + self.cell_scout)):
            return True
        else:
            return False

    def update(self, delta_x, delta_y):
        self.x += delta_x
        self.y += delta_y
        self.rect.x += delta_x
        self.rect.y += delta_y
        

    def draw (self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))

screen = pygame.display.set_mode((SCREEN_H, SCREEN_W))
pygame.display.set_caption(DISPLAY)
bg = pygame.Surface((SCREEN_H, SCREEN_W))
        

def main():
    map_ = []

    for i in range(NUMB_OF_CELLS_H):
        map_.append([])
        for j in range(NUMB_OF_CELLS_W):
            surface = pygame.Surface((CELL_SIZE, CELL_SIZE))
            rand = random.randint(1, 19)
            if rand == 1:
                    color = FOREST
            if 2 <= rand <= 7:
                    color = MOUNTAIN
            if 8 <= rand <= 15:
                    color = GROUND
            if 16 <= rand <= 19:
                    color = WATER
            
            surface.fill(pygame.Color(color))
            map_[i].append(surface)

    scout = Scout(3 * CELL_SIZE, 3 * CELL_SIZE, 2)
    delta_x = 0
    delta_y = 0

    m_loop = True
    while m_loop:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                m_loop = False
                pygame.display.quit()
            if (event.type == pygame.MOUSEBUTTONDOWN):
                scout.click(event.button, event.pos)
    
        for i in range(NUMB_OF_CELLS_H):
            for j in range(NUMB_OF_CELLS_W):
                screen.blit(map_[i][j], (i * CELL_SIZE, j * CELL_SIZE))
    
        scout.update(delta_x, delta_y)
        scout.draw(screen)
        pygame.display.update()

main()
